﻿module FsKeyValueStore.Data

open System

[<Obsolete>]
let inline TODO _ = ()

type ITable<'key> =
    abstract Key: 'key

type Result<'value> =
    | Success of 'value
    | Failure of string

exception FailureException of string

module Result =
    let bind (f: 'a -> Result<'b>) (x: Result<'a>) : Result<'b> =
        match x with
        | Success x -> f x
        | Failure s -> Failure s

    let map (f: 'a -> 'b) (x: Result<'a>) : Result<'b> =
        match x with
        | Success x -> Success (f x)
        | Failure s -> Failure s

    let iter (f: 'a -> unit) (x: Result<'a>) : unit =
        match x with
        | Success x -> f x
        | Failure s -> ()

    let get (x: Result<'a>) : 'a =
        match x with
        | Success x -> x
        | Failure s -> raise <| FailureException s

type ResultBuilder () =
    member x.Bind (v, f) = Result.bind f v
    member x.Return v = Success v
    member x.ReturnFrom o = o
let result = ResultBuilder ()

module Array = 
    let resultMap (f: 'a -> Result<'b>) (xs: 'a []) : Result<'b []> = result {
        let bs = Array.zeroCreate<'b> xs.Length
        let rec loop i = result {
            if i = xs.Length then 
                return ()
            else
                let! b = f xs.[i]
                bs.[i] <- b
                return! loop (i + 1)
        }
        do! loop 0
        return bs
    }

// ----------------------------------------------------------------------------

TODO "Insert shouldnt force user to save"

type IStoreTable<'key, 'value when 'value :> ITable<'key>> =
    //abstract IsValid : 'value -> Result<'value>
    /// Insert the value, returning the key. For now, you will need to save once more.
    abstract Insert : 'value -> Result<'key>
    /// Save the current instance to the table. If the primary key is new, it will create it, otherwise update value.
    abstract Save : 'value -> Result<unit>
    /// Save multiple instances as a transaction. Behaves like Save
    abstract SaveAll : 'value [] -> Result<unit>
    /// Load the instance associated with the specific key.
    abstract Load : 'key -> Result<'value>
    /// Load all instances
    abstract LoadAll : unit -> Result<'value []>
    /// Try to load the value for key
    abstract TryLoad: 'key -> Result<'value option>
    /// Delete the row associated with a key. Returns success even if the key didn't exist.
    abstract Delete : 'key -> Result<unit>
    /// Convert from the type provided to the type of the table. 
    abstract ConvertAll<'oldvalue when 'oldvalue :> ITable<'key>> : ('oldvalue -> 'value) -> Result<unit>

type IStoreDatabase =
    /// Return a table on which you can save/load instances. Create the table if it doesn't exist. 
    /// Might need to create extra tables for foreign key capabilities. Verify type matches table on open.
    abstract OpenTable<'key, 'value when 'value :> ITable<'key>> : unit -> Result<IStoreTable<'key, 'value>>
    /// Cleanup of any of the state needed.
    abstract CloseTable<'key, 'value when 'value :> ITable<'key>> : unit -> Result<unit>
    /// Delete table. Returns success even if the table didn't exist.
    abstract DeleteTable<'key, 'value when 'value :> ITable<'key>> : unit -> Result<unit>

type IStore =
    /// Open a connection to a database. Create the database if it doesn't exist (if possible)
    abstract OpenDatabase : connectionString:string -> Result<IStoreDatabase>
    /// Close a connection to a database.
    abstract CloseDatabase : connectionString:string -> Result<unit>
    /// Delete a database
    abstract DeleteDatabase : connectionString:string -> Result<unit>