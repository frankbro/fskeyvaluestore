﻿module FsKeyValueStore.Sqlite

open System
open System.IO

open Nessos.FsPickler
open System.Data.SQLite

open Data

let getBytes (reader: SQLiteDataReader) : byte [] =
    let CHUNK_SIZE = 2 * 1024
    let buffer = Array.create CHUNK_SIZE 0uy
    let mutable fieldOffset = 0L
    let mutable bytesRead = reader.GetBytes(0, fieldOffset, buffer, 0, buffer.Length)
    use stream = new MemoryStream ()
    while bytesRead > 0L do
        stream.Write(buffer, 0, (int)bytesRead)
        fieldOffset <- fieldOffset + bytesRead
        bytesRead <- reader.GetBytes(0, fieldOffset, buffer, 0, buffer.Length)
    stream.ToArray ()

type KeyType =
    | Int
    | Int64
with
    member x.DbType = 
        match x with
        | Int   -> DbType.Int32
        | Int64 -> DbType.Int64

type SqliteStoreTable<'key, 'value when 'value :> ITable<'key>> (name: string, keyType: KeyType, conn: SQLiteConnection) =
    let binary = FsPickler.CreateBinary ()

    let extractByteArrays (key: 'key) : Result<(byte []) []> =
        try
            let sql = sprintf "SELECT value FROM %s WHERE key=@key" name 
            let command = new SQLiteCommand(sql, conn)
            command.Parameters.Add("@key", keyType.DbType).Value <- key
            let reader = command.ExecuteReader ()
            let results = 
                [|
                    while reader.Read () do
                        yield getBytes reader
                |]
            Success results
        with
        | e -> Failure <| sprintf "Fetching in table for type %s threw an exception: %s" name e.Message

    let save (value: 'value) : Result<unit> =
        try
            let key = (value :> ITable<'key>).Key
            let value = binary.Pickle<'value> value
            let sql = sprintf "INSERT OR REPLACE INTO %s (key, value) VALUES (@key, @value)" name 
            let command = new SQLiteCommand(sql, conn)
            command.Parameters.Add("@key", keyType.DbType).Value <- key
            command.Parameters.Add("@value", DbType.Binary, value.Length * 4).Value <- value
            match command.ExecuteNonQuery () with
            | 1 -> Success ()
            | _ -> Failure "The result of the insert was not 1 row modified."
        with
        | e -> Failure <| sprintf "Inserting or updating in table for type %s threw an exception: %s" name e.Message

    interface IStoreTable<'key, 'value> with
        member x.Insert (value: 'value) : Result<'key> =
            try
                TODO "Make a transaction here"
                let value = binary.Pickle<'value> value
                let sql = sprintf "INSERT INTO %s (value) VALUES (@value)" name
                let command = new SQLiteCommand(sql, conn)
                command.Parameters.Add("@value", DbType.Binary, value.Length * 4).Value <- value
                match command.ExecuteNonQuery () with
                | 1 ->
                    let sql = "SELECT last_insert_rowid()"
                    let command = new SQLiteCommand(sql, conn)
                    let lastId = command.ExecuteScalar ()
                    match keyType with
                    | KeyType.Int   -> 
                        let converted = Convert.ToInt32 lastId :> obj
                        Success (converted :?> 'key)
                    | KeyType.Int64 ->
                        let converted = Convert.ToInt64 lastId :> obj
                        Success (converted :?> 'key)
                | _ -> Failure "The result of the insert was not 1 row modified."
            with
            | e -> Failure <| sprintf "Inserting in table for type %s threw an exception: %s" name e.Message

        member x.Save (value: 'value) : Result<unit> =
            try
                save value
            with
            | e -> Failure <| sprintf "Inserting or updating in table for type %s threw an exception: %s" name e.Message

        member x.SaveAll (values: 'value []) : Result<unit> =
            try
                let transaction = conn.BeginTransaction ()
                let results = 
                    values
                    |> Array.resultMap save
                match results with
                | Failure _ ->
                    transaction.Rollback ()
                    Failure "One of the values provided failed to be saved. Rollback."
                | Success _ ->
                    transaction.Commit ()
                    Success ()
            with
            | e -> Failure <| sprintf "Inserting in table for type %s threw an exception: %s" name e.Message

        member x.Load (key: 'key) : Result<'value> =
            try
                extractByteArrays key
                |> Result.bind (fun byteArrays ->
                    match byteArrays with
                    | [| bytes |] ->
                        let value = binary.UnPickle bytes
                        Success value
                    | _ -> Failure <| sprintf "Not exactly 1 result was returned from a load."
                )
            with
            | e -> Failure <| sprintf "Fetching in table for type %s threw an exception: %s" name e.Message

        member x.LoadAll () : Result<'value []> =
            try
                let sql = sprintf "SELECT value FROM %s" name
                let command = new SQLiteCommand(sql, conn)
                let reader = command.ExecuteReader ()
                let results = 
                    [|
                        while reader.Read () do
                            yield getBytes reader
                    |]
                    |> Array.map (fun bytes -> binary.UnPickle bytes)
                Success results
            with
            | e -> Failure <| sprintf "Fetching in table for type %s threw an exception: %s" name e.Message

        member x.TryLoad (key: 'key) : Result<'value option> =
            try
                extractByteArrays key
                |> Result.bind (fun byteArrays ->
                    match byteArrays with
                    | [||] -> Success None
                    | [| bytes |] ->
                        let value = binary.UnPickle bytes
                        Success (Some value)
                    | _ -> Failure <| sprintf "More than 1 result was returned from a TryLoad"
                )
            with
            | e -> Failure <| sprintf "Fetching in table for type %s threw an exception: %s" name e.Message

        member x.Delete (key: 'key) : Result<unit> =
            try
                let sql = sprintf "DELETE FROM %s WHERE key=@key" name
                let command = new SQLiteCommand(sql, conn)
                command.Parameters.Add("@key", keyType.DbType).Value <- key
                match command.ExecuteNonQuery () with
                | 0 -> Success ()
                | 1 -> Success ()
                | _ -> Failure "The result of the delete was not 0 or 1 row modified."
            with
            | e -> Failure <| sprintf "Deleting in table for type %s threw an exception: %s" name e.Message

        member x.ConvertAll<'oldvalue when 'oldvalue :> ITable<'key>> (converter: 'oldvalue -> 'value) =
            try
                let sql = sprintf "SELECT value FROM %s" name
                let command = new SQLiteCommand(sql, conn)
                let reader = command.ExecuteReader ()
                let results = 
                    [|
                        while reader.Read () do
                            yield getBytes reader
                    |]
                    |> Array.choose (fun bytes -> 
                        try
                            Some <| binary.UnPickle bytes
                        with
                        | e -> None
                    )
                    |> Array.map (fun oldvalue -> converter oldvalue)
                let result = (x :> IStoreTable<_,_>).SaveAll results
                result
            with
            | e -> Failure <| sprintf "Could not convert at least one row from the table"

type SqliteStoreDatabase (conn: SQLiteConnection) =
    let mutable tables : Map<string, SQLiteConnection * KeyType * obj> = Map.empty

    let tableExists (name: string) =
        let sql = sprintf "SELECT name FROM sqlite_master WHERE type='table' AND name='%s'" name
        let command = new SQLiteCommand(sql, conn)
        let result = command.ExecuteReader()
        result.HasRows

    member private x.validateTable<'key, 'value when 'value :> ITable<'key>> (name: string) =
        x.getKeyType<'key, 'value> ()
        |> Result.bind (fun keyType ->
            if not <| tableExists name then
                x.createTable name
                |> Result.map (fun () -> keyType)
            else
                Success keyType
        )

    member private x.getKeyType<'key, 'value when 'value :> ITable<'key>> () : Result<KeyType> =
        match typeof<'key> with
        | t when t = typeof<int> -> Success (Int)
        | t when t = typeof<int64> -> Success (Int64)
        | t -> Failure <| sprintf "The key type can only be int or int64, here is type %A" t

    member private x.createTable<'key, 'value when 'value :> ITable<'key>> (name: string) : Result<unit> =
        try
            TODO "Allow not having autoincrement and setting not null"
            let sql = sprintf "CREATE TABLE %s (key INTEGER PRIMARY KEY AUTOINCREMENT, value BLOB NOT NULL)" name
            let command = new SQLiteCommand(sql, conn)
            TODO "Figure out the return of this, sometimes it's 0, sometimes it's 1"
            (*
            match command.ExecuteNonQuery () with
            | 0 -> Success ()
            | s -> Failure "The result of the table creation was not 0 row modified."
            *)
            command.ExecuteNonQuery () |> ignore<int>
            Success ()
        with
        | e -> Failure <| sprintf "Creating the table for type %s threw an exception: %s" name e.Message

    interface IStoreDatabase with
        member x.OpenTable<'key, 'value when 'value :> ITable<'key>> () =
            if conn.State <> ConnectionState.Open then Failure "Connection is closed" else
            let t = typeof<'value>
            let name = t.Name
            tables
            |> Map.tryFind name
            |> (function
                | Some (_, _, table) ->
                    Success (table :?> IStoreTable<'key, 'value>)
                | None ->
                    x.validateTable<'key, 'value> name
                    |> Result.bind (fun validationResult ->
                        let table = SqliteStoreTable<'key, 'value>(name, validationResult, conn)
                        tables <- Map.add name (conn, validationResult, table :> obj) tables
                        Success (table :> IStoreTable<'key, 'value>)
                    )
            )

        member x.CloseTable<'key, 'value when 'value :> ITable<'key>> () =
            let t = typeof<'value>
            tables <- Map.remove t.Name tables
            Success ()

        member x.DeleteTable<'key, 'value when 'value :> ITable<'key>> () =
            if conn.State <> ConnectionState.Open then Failure "Connection is closed" else
            let t = typeof<'value>
            let name = t.Name
            try
                let sql = sprintf "DROP TABLE IF EXISTS %s" name
                let command = new SQLiteCommand(sql, conn)
                command.ExecuteNonQuery () |> ignore<int>
                tables <- Map.remove name tables
                Success ()
            with
            | e -> Failure <| sprintf "Deleting the table for type %s threw an exception: %s" name e.Message

type SqliteStore () =
    let mutable databases : Map<string, SQLiteConnection * SqliteStoreDatabase> = Map.empty

    interface IStore with
        member x.OpenDatabase connectionString =
            let database = 
                match Map.tryFind connectionString databases with
                | Some (_, database) -> 
                    database
                | None ->
                    let conn = new SQLiteConnection(sprintf "Data Source=%s;Version=3;" connectionString)
                    conn.Open ()
                    let database = new SqliteStoreDatabase(conn)
                    databases <- Map.add connectionString (conn, database) databases
                    database
            Success (database :> IStoreDatabase)

        member x.CloseDatabase connectionString =
            match Map.tryFind connectionString databases with
            | None -> Success ()
            | Some (conn, _) ->
                conn.Close ()
                conn.Dispose ()
                databases <- Map.remove connectionString databases
                Success ()

        member x.DeleteDatabase connectionString =
            try
                match Map.tryFind connectionString databases with
                | None -> 
                    ()
                | Some (conn, _) ->
                    conn.Close ()
                    conn.Dispose ()
                File.Delete connectionString
                databases <- Map.remove connectionString databases
                Success ()
            with
            | e -> Failure <| sprintf "Deleting the database `%s` threw an exception: %s" connectionString e.Message