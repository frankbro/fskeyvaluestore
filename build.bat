set config=%1
if "%config%" == "" (
   set config=Release
)

set version=1.0.0
if not "%PackageVersion%" == "" (
   set version=%PackageVersion%
)

set nuget=
if "%nuget%" == "" (
	set nuget=nuget
)

REM Package restore
call %nuget% install FsKeyValueStore\packages.config -OutputDirectory %cd%\packages -Prerelease -NonInteractive
call %nuget% install FsKeyValueStore.Test\packages.config -OutputDirectory %cd%\packages -Prerelease -NonInteractive

REM Build
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\msbuild FsKeyValueStore.sln /p:Configuration="%config%" /m /v:M /fl /flp:LogFile=msbuild.log;Verbosity=diag /nr:false

REM Package
mkdir Build
mkdir Build\lib
mkdir Build\lib\net40

call %nuget% pack "FsKeyValueStore.nuspec" -NoPackageAnalysis -verbosity detailed -o Build -Version %version% -p Configuration="%config%"
