﻿module FsKeyValue.Test.MsSql

open FsKeyValueStore.Data
open FsKeyValueStore.MsSql

open Microsoft.VisualStudio.TestTools.UnitTesting

//-----------------------------------------------------------------------------
let inline (=?) (a: 't when 't : equality) (b: 't when 't : equality) = 
    if a <> b then
        raise <| AssertFailedException(sprintf "A =? B is false.\nA =\n%A\nB =\n%A" a b)

//-----------------------------------------------------------------------------

type Simple = {
    Id: int
    String: string
    Float: float
}
with
    interface ITable<int> with
        member x.Key = x.Id

module Old = 
    type Simple = {
        Id: int
        String: string
    }
    with 
        interface ITable<int> with
            member x.Key = x.Id

[<TestClass>]
type ``MsSql tests`` () =
    let store = MsSqlStore() :> IStore

    let name = System.IO.File.ReadAllLines("ConnectionString.txt").[0]

    [<TestMethod>]
    member x.``Save and Load`` () =
        let simple = { Id = 0; String = "String"; Float = 3.14 }
        let result = result {
            let! database = store.OpenDatabase name
            do! database.DeleteTable<int, Simple> ()
            let! table = database.OpenTable<int, Simple> ()
            do! table.Save simple
            let! result = table.Load simple.Id
            return result
        }
        Success simple =? result
        ()

    [<TestMethod>]
    member x.``TryLoad, Save and TryLoad`` () =
        let simple = { Id = 0; String = "String"; Float = 3.14 }
        let before = result {
            let! database = store.OpenDatabase name
            do! database.DeleteTable<int, Simple> ()
            let! table = database.OpenTable<int, Simple> ()
            let! result = table.TryLoad simple.Id
            return result
        }
        Success None =? before
        let after = result {
            let! database = store.OpenDatabase name
            let! table = database.OpenTable<int, Simple> ()
            do! table.Save simple
            let! result = table.TryLoad simple.Id
            return result
        }
        Success (Some simple) =? after
        ()

    [<TestMethod>]
    member x.``SaveAll and LoadAll`` () =
        let simples =
            [|
                { Id = 1; String = "1"; Float = 1.0 }
                { Id = 2; String = "2"; Float = 2.0 }
                { Id = 3; String = "3"; Float = 3.0 }
                { Id = 4; String = "4"; Float = 4.0 }
                { Id = 5; String = "5"; Float = 5.0 }
            |]
        let result = result {
            let! database = store.OpenDatabase name
            do! database.DeleteTable<int, Simple> ()
            let! table = database.OpenTable<int, Simple> ()
            do! table.SaveAll simples
            let! result = table.LoadAll ()
            return result
        }
        Success simples =? result
        ()

    [<TestMethod>]
    member x.``Save, Delete and TryLoad`` () =
        let simple = { Id = 0; String = "String"; Float = 3.14 }
        let before = result {
            let! database = store.OpenDatabase name
            do! database.DeleteTable<int, Simple> ()
            let! table = database.OpenTable<int, Simple> ()
            do! table.Save simple
            do! table.Delete simple.Id
            let! result = table.TryLoad simple.Id
            return result
        }
        Success None =? before
        ()
        
    [<TestMethod>]
    member x.``Insert and Load`` () =
        let simple = { Id = -1; String = "String"; Float = 3.14 }
        let result = result {
            let! database = store.OpenDatabase name
            do! database.DeleteTable<int, Simple> ()
            let! table = database.OpenTable<int, Simple> ()
            let! key = table.Insert simple
            TODO "Once we fix our way to do insert, remove this"
            do! table.Save { simple with Id = key }
            let! result = table.Load key
            return result
        }
        match result with
        | Failure s -> raise <| AssertFailedException (s)
        | Success loadedSimple ->
            simple.String =? loadedSimple.String
            simple.Float  =? loadedSimple.Float
            ()

    [<TestMethod>]
    member x.``SaveAll, ConvertAll and LoadAll`` () =
        let oldSimples : Old.Simple [] = 
            [|
                { Id = 1; String = "1"; }
                { Id = 2; String = "2"; }
                { Id = 3; String = "3"; }
                { Id = 4; String = "4"; }
                { Id = 5; String = "5"; }
            |]
        let convert (old: Old.Simple) = 
            { Id = old.Id; String = old.String; Float = float old.Id }
        let simples = 
            oldSimples 
            |> Array.map convert
        let empty = result {
            let! database = store.OpenDatabase name
            do! database.DeleteTable<int, Old.Simple> ()
            let! table = database.OpenTable<int, Old.Simple> ()
            do! table.SaveAll oldSimples
            do! database.CloseTable<int, Old.Simple> ()
            return ()
        }
        let result = result {
            let! database = store.OpenDatabase name
            let! table = database.OpenTable<int, Simple> ()
            do! table.ConvertAll convert
            let! result = table.LoadAll ()
            return result
        }
        Success simples =? result
        ()
